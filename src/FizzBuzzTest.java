import static org.junit.Assert.*;

import org.junit.Test;


public class FizzBuzzTest {
	

	/**
	 * La chiamata al metodo fizzbuzzSequence con parametro intero 1,
	 * deve restituire la stringa "1".
	 */
	FizzBuzzer fizzbuzzer = new FizzBuzzer();

	@Test
	public void fizzbuzzSequence1() {
		assertEquals("1", fizzbuzzer.fizzbuzzSequence(1));
	}
	
	@Test
	public void fizzbuzzSequence2(){
		assertEquals("1 2", fizzbuzzer.fizzbuzzSequence(2));
	}
	
	@Test
	public void fizzbuzzSequence3(){
		assertEquals("1 2 fizz", fizzbuzzer.fizzbuzzSequence(3));
	}
	
	@Test
	public void fizzbuzzSequence5(){
		fizzbuzzer.fizzbuzzSequence(5).endsWith("4 buzz");
	}
	
	@Test
	public void fizzbuzzSequence6(){
		fizzbuzzer.fizzbuzzSequence(6).endsWith(" fizz");
	}
	
	@Test
	public void fizzbuzzSequence10(){
		fizzbuzzer.fizzbuzzSequence(10).endsWith("fizz buzz");
	}
	
	@Test
	public void fizzbuzzSequence15(){
		fizzbuzzer.fizzbuzzSequence(15).endsWith("fizzbuzz");
	}
	
	@Test
	public void fizzbuzzSequence100(){
		fizzbuzzer.fizzbuzzSequence(100).endsWith("98 fizz buzz");
	}
	
	@Test
	public void fizzbuzzSequenceNeg(){
		assertEquals("non accetto numeri negativi", fizzbuzzer.fizzbuzzSequence(-5));
	}
	
	@Test
	public void fizzbuzzSequence7(){
		fizzbuzzer.fizzbuzzSequence(7).endsWith(" woof");
	}
	
	@Test
	public void fizzbuzzSequence35(){
		fizzbuzzer.fizzbuzzSequence(35).endsWith(" fizzbuzzwoof");
	}
	
	@Test
	public void fizzbuzzSequence53(){
		fizzbuzzer.fizzbuzzSequence(53).endsWith(" fizzbuzz");
	}
}
