public class FizzBuzzer implements IfizzBuzz{

	@Override
	public String fizzbuzzSequence(int number){
		
		String listanumerifinale = "";
		
		try{
		
			verificoEccezioni(number);
			
			listanumerifinale = composizioneStringa(number);

		}catch(IllegalArgumentException e){			
			return "non accetto numeri negativi";
		}
		return listanumerifinale;
		
	}

	
	public void verificoEccezioni(int number) throws IllegalArgumentException{
		if(number <= 0)
			throw new IllegalArgumentException();	
	}
	
	public String composizioneStringa(int number) throws IllegalArgumentException{
		
		String compnumeri = "";
		
		for (int i=1; i<= number; i++){		
			compnumeri += controlloNumero(i);
		}
		
		return  compnumeri.substring(1);
	}
	
	
	public String controlloNumero(int numero) throws IllegalArgumentException{
		
		String elenconumeri = " ";
		
		if(numero % 3 == 0 || String.valueOf(numero).contains("3"))
			elenconumeri +="fizz";
		if(numero % 5 == 0 || String.valueOf(numero).contains("5"))
			elenconumeri +="buzz";
		if(numero % 7 == 0)
			elenconumeri +="woof";
		if(elenconumeri.equals(" "))
			elenconumeri += numero;
		
		return elenconumeri;
	}
	
}
